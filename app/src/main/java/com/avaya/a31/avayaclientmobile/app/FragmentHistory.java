package com.avaya.a31.avayaclientmobile.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class FragmentHistory extends Fragment{
    private ArrayList<NameValuePair> nameValuePairs; //Used for post request
    private List<Contact> historyContacts;
    private Integer historyCount;
    ListView historyListView;
    MainActivity mainActivity;
    ArrayAdapter<Contact> historyListViewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle saveInstanceState){
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        mainActivity = (MainActivity) getActivity();

        historyListView = (ListView) view.findViewById(R.id.historyListView);
        historyListViewAdapter = new HistoryListViewAdapter(getActivity().getBaseContext());
        historyListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                PublicMethods.hideScopiaInbox(mainActivity);
                return false;
            }
        });
        initHistoryList();
        return view;
    }

    public void initHistoryList(){
        historyContacts = new ArrayList<Contact>();
        historyContacts = mainActivity.contactsDB.getAllRecentsForUser(mainActivity.userHandle);
        historyCount = historyContacts.size()-1;  // Count minus 1
        historyListView.setAdapter(historyListViewAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.history_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.clear_history) {
            mainActivity.contactsDB.deleteAllRecentForUser(mainActivity.userHandle);
            Toast.makeText(mainActivity.getBaseContext(), R.string.history_cleared, Toast.LENGTH_SHORT).show();
            initHistoryList();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class HistoryListViewAdapter extends ArrayAdapter<Contact>{
        LayoutInflater inflater;

        private View.OnClickListener callButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = historyListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    startActivity(IntentMaker.makeCallIntent(historyContacts.get(historyCount-position).getExtension()));
                }
            }
        };

        private View.OnClickListener emailButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = historyListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    startActivity(IntentMaker.makeEmailIntent(historyContacts.get(historyCount-position).getEmail()));
                }
            }
        };

        private View.OnClickListener scopiaButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = historyListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    ArrayList<Contact> recipients = new ArrayList<Contact>();
                    recipients.add(historyContacts.get(historyCount-position));
                    mainActivity.makeScopiaRequest(recipients);
                }
            }
        };
        public HistoryListViewAdapter(Context context){//The constructor
            super(context,R.layout.cell_ldap_search,historyContacts);
            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        }

        @Override
        public int getCount() {
            return historyContacts.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View cellView = convertView;

            if (cellView == null){
                cellView = inflater.inflate(R.layout.cell_history, parent, false);
            }

            Contact contactEntry = historyContacts.get(historyCount-position);
            //Fill the cellView
            TextView displayName = (TextView) cellView.findViewById(R.id.recentDisplayName);
            displayName.setText(contactEntry.getGivenName()+" "+contactEntry.getFamilyName());
            ImageView directionIndicator = (ImageView) cellView.findViewById(R.id.imageDirection);
            if (contactEntry.isDirection()){
                directionIndicator.setImageResource(R.drawable.direction_right);
            }else{
                directionIndicator.setImageResource(R.drawable.direction_left);
            }

            TextView callTime = (TextView) cellView.findViewById(R.id.recentCallTime);
            callTime.setText(PublicMethods.dateToDisplayString(contactEntry.getCallTime()));

            // Set on click listeners
            ImageButton callButton = (ImageButton) cellView.findViewById(R.id.recentCallButton);
            ImageButton emailButton = (ImageButton) cellView.findViewById(R.id.recentEmailButton);
            if(contactEntry.getEmail().length()<5){
                emailButton.setEnabled(false);// Disable the button if there is not valid email
            }
            ImageButton scopiaButton = (ImageButton) cellView.findViewById(R.id.recentScopiaButton);
            callButton.setOnClickListener(callButtonClickListener);
            emailButton.setOnClickListener(emailButtonClickListener);
            scopiaButton.setOnClickListener(scopiaButtonClickListener);

            return cellView;
        }
    }
}
