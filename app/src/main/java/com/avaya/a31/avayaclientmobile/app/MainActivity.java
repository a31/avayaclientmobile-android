package com.avaya.a31.avayaclientmobile.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.jar.JarFile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

public class MainActivity extends ActionBarActivity implements ActionBar.TabListener{

    SectionsPagerAdapter mSectionsPagerAdapter;
    //Declare variables/properties
    public ViewPager mViewPager;
    public ActionBar actionBar;
    public Fragment fragmentScopiaInbox;
    public String userExtension;
    public String userFirstName;
    public String userLastName;
    public String userHandle;
    public String userEmail;
    private String scopiaRequestStr;
    SharedPreferences userInfo;
    public DBAdapter contactsDB;
    public ArrayList<Contact> scopiaRecipients;
    public Boolean connectionSuccess;
    public Boolean scopiaRequestSuccess;
    public Boolean scopiaInboxConnectionSuccess;
    public Boolean scopiaInboxReceived;
    public Boolean scopiaInboxReceivedSuccess;
    public JSONArray scopiaInboxMessages;
    public List<Contact> newCallers;
    public Boolean logoutSuccess;
    MainActivity mainActivityContext;
    public ProgressBar spinner;
    public static final int UDP_PORT = 7000;
    public DatagramSocket udpSocket;
    public Handler udpHandler;
    public Thread udpThread;
    public Boolean udpSwitch; // Set udpSwitch to false to stop listening
    public String invitePartExtension;
    public Double newVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (ProgressBar)findViewById(R.id.progress_bar_main);
        spinner.setVisibility(View.GONE);

        // Set up the action bar.
        actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);// Keep 3 background pages before destroying

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
        init();
    }
    public void init(){
        userInfo = getSharedPreferences(getString(R.string.user_info), Context.MODE_PRIVATE);
        userExtension = userInfo.getString("userExtension", "0"); //Return a single zeros by default;
        userFirstName = userInfo.getString("userFirstName", "0");
        userLastName = userInfo.getString("userLastName", "0");
        userHandle = userInfo.getString("userHandle", "0");
        userEmail = userInfo.getString("userEmail", "0");
        mainActivityContext = this;
        scopiaRecipients = new ArrayList<Contact>();
                // Instantiate and open the database
        contactsDB = new DBAdapter(getBaseContext());
        contactsDB.open();

        fragmentScopiaInbox = getSupportFragmentManager().findFragmentById(R.id.fragment_scopia_inbox_fragment);
        FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
        ft.hide(fragmentScopiaInbox).commit();

        udpSwitch = true;
        udpHandler = new Handler();
        udpThread = new Thread(new UDPRunnable());
        udpThread.start();// Start the UDP connection
        new UpdateCheckAsyncTask().execute();
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first
        startService(IntentMaker.makeRegistrationIntent(mainActivityContext));
        ScopiaInboxAsyncTask scopiaInboxAsyncTask = new ScopiaInboxAsyncTask();
        scopiaInboxAsyncTask.execute();
        // TODO: resume the udpThread
    }

    @Override
    public void onPause()
    {
        super.onPause();
        // TODO: pause
        //udpSwitch=false;
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        contactsDB.close(); // Close the database
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        int id = item.getItemId();
        if (id == R.id.my_scopia_inbox) {
            fragmentScopiaInbox = getSupportFragmentManager().findFragmentById(R.id.fragment_scopia_inbox_fragment);
            FragmentTransaction ft= getSupportFragmentManager().beginTransaction();
            if (fragmentScopiaInbox != null && fragmentScopiaInbox.isAdded()){
                if(fragmentScopiaInbox.isHidden()){
                    ft.show(fragmentScopiaInbox);
                    new ScopiaInboxAsyncTask().execute();
                }else{
                    ft.hide(fragmentScopiaInbox);
                    updateScopiaInbox();
                }
                ft.commit();
            }
            return true;
        }
        if (id == R.id.my_scopia_room) {
            if (userExtension.length()>5){
                openScopiaLink(userExtension);
            }else{
                Toast.makeText(getBaseContext(), R.string.login_again, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        if (id == R.id.schedule_scopia_meeting) {
            if (scopiaRecipients.size()>0){
                //FragmentTransaction ft= .beginTransaction();
                FragmentDatePicker datePickerDialog = new FragmentDatePicker();
                datePickerDialog.show(getSupportFragmentManager(),"datePickerTag");
            }else{
                Toast.makeText(getBaseContext(), R.string.no_contact_selected, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        if (id == R.id.logout_action) {
            spinner.setVisibility(View.VISIBLE);
            SharedPreferences.Editor spEdit = userInfo.edit();
            spEdit.clear();
            spEdit.commit();

            startService(IntentMaker.makeUnregistrationIntent(mainActivityContext));
            new LogoutAsyncTask().execute();

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean scopiaInstalled(){
        PackageManager pm = getPackageManager();
        boolean appInstalled; // is false by default
        try{
            pm.getPackageInfo(getString(R.string.scopia_uri),PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        }catch (PackageManager.NameNotFoundException exception){
            appInstalled=false;
        }
        return appInstalled;
    }
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {}
    @Override
    public void onBackPressed() {finish();}

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0){
                fragment = new FragmentMyContacts();
            }else if(position == 1){
                fragment = new FragmentHistory();
            }else if(position==2){
                fragment = new FragmentAvayaDirectory();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_my_contacts).toUpperCase(l);
                case 1:
                    return getString(R.string.title_history).toUpperCase(l);
                case 2:
                    return getString(R.string.title_avaya_directory).toUpperCase(l);
            }
            return null;
        }
    }
    // ---------------- Interface methods ------------------------
    public void updateContacts() {
        FragmentMyContacts frag = (FragmentMyContacts)
                getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":0");
        if(frag != null){  // could be null if not instantiated yet
            if (frag.getView() != null) {
                frag.initExpandableView();
            }
        }
    }
    public void updateRecent() {
        FragmentHistory frag = (FragmentHistory)
                getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":1");
        if(frag != null){  // could be null if not instantiated yet
            if (frag.getView() != null) {
                frag.initHistoryList();
            }
        }
    }

    public void updateScopiaInbox() {
        FragmentScopiaInbox frag = (FragmentScopiaInbox)
                getSupportFragmentManager().findFragmentById(R.id.fragment_scopia_inbox_fragment);
        if(frag != null){  // could be null if not instantiated yet
            if (frag.getView() != null) {
                if (newCallers!=null){
                    if (newCallers.size()>0){
                        frag.initScopiaInboxListView();
                        PublicMethods.showScopiaInbox(mainActivityContext);
                        updateRecent();
                        new ScopiaInboxConfirmAsyncTask().execute();
                    }else{
                        frag.updateScopiaInboxListView();
                    }
                }else{
                    frag.updateScopiaInboxListView();
                }
            }
        }
    }

    public void addRecipient(Contact contact){
        if(scopiaRecipients.contains(contact)){
            Toast.makeText(getBaseContext(),R.string.recipient_already_selected,Toast.LENGTH_SHORT).show();
        }else {
            scopiaRecipients.add(contact);
            Toast.makeText(getBaseContext(), R.string.recipient_selected, Toast.LENGTH_SHORT).show();
        }
    }
    public void removeRecipient(Contact contact){
        if (scopiaRecipients.remove(contact)){
            Toast.makeText(getBaseContext(),R.string.recipient_removed,Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(getBaseContext(),R.string.recipient_already_removed,Toast.LENGTH_SHORT).show();
        }
    }
    // --------------------------- Helper methods -----------------------------------

    public void setInvitePartExtension(String invitePartExtension) {
        this.invitePartExtension = invitePartExtension;
    }

    public void openScopiaLink(String extensionStr){

        Uri uri;
        if(scopiaInstalled()) {
            uri = Uri.parse(getString(R.string.scopia_prefix).concat(extensionStr));
        }else{
            uri = Uri.parse(getString(R.string.scopia_http_prefix).concat(extensionStr));
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        // ------ Set this flag to open a new window ------
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        // ------ Set this flag to open a new window ------
        startActivity(intent);
    }

    // -------------------------- Methods for network connections ---------------------
    public void makeScopiaRequest(ArrayList<Contact> recipients){
        spinner.setVisibility(View.VISIBLE);
        scopiaRequestStr = getString(R.string.scopia_invite_prefix).concat(userExtension);
        for (Contact recipient : recipients) {
            scopiaRequestStr = scopiaRequestStr.concat(getString(R.string.scopia_invite_insert_receiver)).concat(recipient.getExtension());
            contactsDB.addRecent(userHandle,recipient,true,PublicMethods.dateToString(Calendar.getInstance().getTime()));
        }
        scopiaRequestStr = scopiaRequestStr.concat(getString(R.string.scopia_invite_insert_firstname)).concat(userFirstName);
        scopiaRequestStr = scopiaRequestStr.concat(getString(R.string.scopia_invite_insert_lastname)).concat(userLastName);
        ScopiaRequestAsyncTask requestTask = new ScopiaRequestAsyncTask();
        updateRecent();
        requestTask.execute();
    }

    public class ScopiaRequestAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                connectionSuccess= false;
                scopiaRequestSuccess = false;
                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpGet httpGet = new HttpGet(scopiaRequestStr);
                HttpResponse httpResponse = httpClient.execute(httpGet);

                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    connectionSuccess= true;
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);
                        //String authStatus = jsonResponse.getString("status");
                        if (jsonResponse.getInt("status")==0){
                            //Write all user information in before entering the app
                            scopiaRequestSuccess = true;
                        }
                    }
                }
            }catch(Exception exception){
                exception.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void unused){
            spinner.setVisibility(View.GONE);
            if (scopiaRequestSuccess){
                // Invite user to his/her own room
                PublicMethods.makeOpenScopiaRoomAlertDialog(mainActivityContext).show();
            }else{
                if (connectionSuccess){
                    Toast.makeText(getBaseContext(),R.string.server_error_scopia,Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getBaseContext(),R.string.connection_error_scopia,Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public class ScopiaInboxAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                scopiaInboxConnectionSuccess= false;
                scopiaInboxReceived = false;
                scopiaInboxReceivedSuccess=false;
                newCallers = new ArrayList<Contact>();

                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpGet httpGet = new HttpGet(getString(R.string.scopia_message_url)+userExtension);
                HttpResponse httpResponse = httpClient.execute(httpGet);

                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    scopiaInboxConnectionSuccess= true;
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);
                        //String authStatus = jsonResponse.getString("status");
                        if (jsonResponse.getInt("status")==0&&jsonResponse.getInt("length")>0){
                            scopiaInboxReceived = true;
                            try{
                                scopiaInboxMessages = jsonResponse.getJSONArray("messages");
                                newCallers = new ArrayList<Contact>();
                                for (int i=0; i<scopiaInboxMessages.length();i++){
                                    try{
                                        JSONObject jsonContact = scopiaInboxMessages.getJSONObject(i);
                                        newCallers.add(new Contact("", jsonContact.getString("sender"),jsonContact.getString("firstName"),jsonContact.getString("lastName"),"", "", jsonContact.getString("sendDate")));
                                    }catch (Exception exception){
                                        scopiaInboxReceivedSuccess = false;
                                    }
                                }
                                scopiaInboxReceivedSuccess = true;

                            }catch(Exception exception){
                                scopiaInboxReceivedSuccess = false;
                            }
                        }
                    }
                }
            }catch(Exception exception){
                exception.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void unused){
            //spinner.setVisibility(View.GONE);
            if (scopiaInboxReceived){
                // Invite user to his/her own room
                updateScopiaInbox();
            }else{
                if (scopiaInboxConnectionSuccess){
                    updateScopiaInbox();
                    //Toast.makeText(getBaseContext(),R.string.missing_messages,Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getBaseContext(),R.string.connection_error_scopia,Toast.LENGTH_SHORT).show();
                }
            }
        }

    }

    public class ScopiaInboxConfirmAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("receiver",userExtension));

                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpPost httpPost = new HttpPost(getString(R.string.scopia_message_received_confirm_url));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                httpClient.execute(httpPost);
            }catch(Exception exception){
                exception.printStackTrace();
                Toast.makeText(getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }

    public class UpdateCheckAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpGet httpGet = new HttpGet(getString(R.string.update_check_url));
                HttpResponse httpResponse = httpClient.execute(httpGet);

                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);
                        //String authStatus = jsonResponse.getString("status");
                        if (jsonResponse.getInt("status")==0){
                            try{
                                String newVersionStr = jsonResponse.getString("android_version");
                                newVersion = Double.valueOf(newVersionStr.trim()).doubleValue();
                            }catch(Exception exception){}
                        }
                    }
                }
            }catch(Exception exception){
                exception.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void unused){
            //spinner.setVisibility(View.GONE);
            if (newVersion != null){
                if (newVersion>PublicMethods.getVersion(mainActivityContext)){
                    PublicMethods.makeUpdateAlertDialog(mainActivityContext).show();
                    newVersion = null;
                }
            }
        }
    }

    public class LogoutAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                logoutSuccess=false;
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("extension",userExtension));
                nameValuePairs.add(new BasicNameValuePair("type","android"));

                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpPost httpPost = new HttpPost(getString(R.string.logout_url));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    logoutSuccess = true;
                }

            }catch(Exception exception){
                exception.printStackTrace();
                Toast.makeText(getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (logoutSuccess){
                spinner.setVisibility(View.GONE);
                Toast.makeText(getBaseContext(),R.string.logout_succeed,Toast.LENGTH_SHORT).show();
                Intent loginActivityIntent = new Intent(mainActivityContext,LoginActivity.class);
                startActivity(loginActivityIntent);
                finish();
            }else{
                Toast.makeText(getBaseContext(),R.string.logout_failed,Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class UDPRunnable implements Runnable {
        private String receivedMessage;
        @Override
        public void run() {
            while(udpSwitch){
                try {
                    Thread.sleep(200);
                    udpSocket = new DatagramSocket(UDP_PORT);
                    byte[] buffer = new byte[1024];
                    DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                    udpSocket.receive(packet);
                    udpSocket.close(); //Need to close the connection.
                    receivedMessage = new String(buffer, 0, packet.getLength());
                }catch(Exception exception){
                    //Log.d("Interrupt Exception","Interrupt exception happened");
                }finally {
                    if (receivedMessage!= null){
                        if (receivedMessage.length()>10){
                            if (receivedMessage.substring(0,10).equals("[dt_SCOPIA")){
                                udpHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        //Display the dialog message
                                        newCallers = new ArrayList<Contact>();
                                        String[] separatedUdpMessage = receivedMessage.split(" ");// separatedUdpMessage[2] [3] [4] Sender/ firstname lastname
                                        Contact tempContact = contactsDB.getContactWithExtension(userHandle, separatedUdpMessage[2]);
                                        if (tempContact == null){
                                            tempContact= new Contact("",separatedUdpMessage[2],separatedUdpMessage[3],separatedUdpMessage[4],"","");
                                        }
                                        contactsDB.addRecent(userHandle,tempContact,false,PublicMethods.dateToString(Calendar.getInstance().getTime()));
                                        new ScopiaInboxConfirmAsyncTask().execute();
                                        PublicMethods.makeScopiaInvitationReceivedAlertDialog(mainActivityContext,receivedMessage).show();
                                        updateRecent();
                                        updateScopiaInbox();

                                    }
                                });
                            }
                        }
                    }
                }
            }
        }
    }

    public void installUpdateApk(){
        new InstallTask(mainActivityContext,getString(R.string.apk_url)).execute();
    }

    class InstallTask extends AsyncTask<Void, Void, String> {
        ProgressDialog mProgressDialog;

        Context context;
        String url;

        public InstallTask(Context context, String url) {
            this.context = context;

            this.url = url;

        }

        protected void onPreExecute() {
            mProgressDialog = ProgressDialog.show(context,
                    "Download", " Downloading in progress..");
        }

        private String downloadapk() {
            String result = "";
            try {
                URL url = new URL(this.url);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                urlConnection.connect();

                File sdcard = Environment.getExternalStorageDirectory();
                File file = new File(sdcard, getString(R.string.install_file_name));

                FileOutputStream fileOutput = new FileOutputStream(file);
                InputStream inputStream = urlConnection.getInputStream();

                byte[] buffer = new byte[1024];
                int bufferLength = 0;

                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);
                }
                fileOutput.close();
                result = "done";

            } catch (Exception exception) {}
            return result;
        }

        protected String doInBackground(Void... params) {
            String result = downloadapk();
            return result;
        }

        protected void onPostExecute(String result) {
            if (result.equals("done")) {
                mProgressDialog.dismiss();
                if (isApkCorrupted()){
                    Toast.makeText(context, R.string.bad_apk_file,Toast.LENGTH_LONG).show();
                }else{
                    installApk();
                }
            } else {
                Toast.makeText(context, R.string.download_error,Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        }

        private void installApk() {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = Uri.fromFile(new File("/sdcard/"+getString(R.string.install_file_name)));
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }

        private boolean isApkCorrupted() {
            boolean corruptedApkFile = false;
            try {
                new JarFile(new File("/sdcard/"+getString(R.string.install_file_name)));
            } catch (Exception ex) {
                corruptedApkFile = true;
            }
            return corruptedApkFile;
        }

    }

}