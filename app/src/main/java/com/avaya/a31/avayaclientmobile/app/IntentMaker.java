package com.avaya.a31.avayaclientmobile.app;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;

import java.util.Date;
import java.util.List;

public class IntentMaker extends Activity {
    public static Intent makeCallIntent(String extension){
        String callNumberStr = "tel:".concat(extension);
        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(callNumberStr));
        return callIntent;
    }
    public static Intent makeEmailIntent(String email){
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",email, null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
        return emailIntent;
    }

    public static Intent makeScheduleEmailIntent(List<Contact> recepients, String userScopialink, Date date,
                                                 String userFirstname, String userLastname, String userExtension,String userEmail){

        String dateStr = PublicMethods.dateToDisplayString(date);

        String[] emails = new String[recepients.size()];
        for(int i = 0; i<recepients.size();i++){
            emails[i] = recepients.get(i).getEmail();
        }
        Spanned htmlBody = Html.fromHtml(new StringBuilder()
                .append("<p>Dear all,<br><br>I would very much like to meet on Scopia with you atyour earliest convenience regarding ______________.</p>")
                .append(String.format("<p>Please use the link below to attend the meeting on <b>%s</b></p>",dateStr))
                .append(String.format("<p><a href = '%s'>%s</a></p>",userScopialink,userScopialink))
                .append(String.format("<p>You may reach me by phone at %s or via email at <a href ='mailto:%s'>%s</a>. </p>",userExtension,userEmail,userEmail))
                .append("<p>Thank you for your attention to this matter. <br><p><o:p>&nbsp;</o:p></p>")
                .append(String.format("<br>Very truly,<br>%s</p>",userFirstname+" "+userLastname))
                .append("<p><o:p>&nbsp;</o:p></p>")
                .append("<p>Created with Avaya Client Mobile Android- IT Innovation</p>")
                .append("<p><a href = 'http://itinnovation.avaya.com/avaya-client-for-mobile/'>http://itinnovation.avaya.com/avaya-client-for-mobile/</a></p>")
                .toString());

        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/html");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, String.format("Scopia meeting on %s",dateStr));
        emailIntent.putExtra(Intent.EXTRA_EMAIL, emails);
        emailIntent.putExtra(Intent.EXTRA_TEXT,htmlBody); // Put html body in the email.
        return emailIntent;
    }

    public static Intent makeRegistrationIntent(Context mainActivityContext){
        Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
        registrationIntent.putExtra("app", PendingIntent.getBroadcast(mainActivityContext,0,new Intent(),0));
        registrationIntent.putExtra("sender","709498547878");
        return registrationIntent;
    }

    public static Intent makeUnregistrationIntent(Context mainActivityContext){
        Intent unregistrationIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
        unregistrationIntent.putExtra("app", PendingIntent.getBroadcast(mainActivityContext,0,new Intent(),0));
        return unregistrationIntent;
    }

    /* ---------- GCM credential information ---------------
    "client_secret":"VelMAO1HY73bgbhvZfrmK5A7",
    "token_uri":"https://accounts.google.com/o/oauth2/token",
    "client_email":"709498547878-fl2m3lv821bf3j8bcn4n0easb8bb63og@developer.gserviceaccount.com",
    "client_x509_cert_url":"https://www.googleapis.com/robot/v1/metadata/x509/709498547878-fl2m3lv821bf3j8bcn4n0easb8bb63og@developer.gserviceaccount.com",
    "client_id":"709498547878-fl2m3lv821bf3j8bcn4n0easb8bb63og.apps.googleusercontent.com",
    "auth_provider_x509_cert_url":"https://www.googleapis.com/oauth2/v1/certs",

    "Project Number(SenderID)": "709498547878",
    "API key":"AIzaSyDnXSYlH8nxfLOA0cucGnM2pXFGoDhgb5w",
    "Activated by":"a31.itinnovation@gmail.com"}
     ------------------------------------------------------*/

}


