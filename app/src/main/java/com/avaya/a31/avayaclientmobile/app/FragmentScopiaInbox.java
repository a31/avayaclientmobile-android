package com.avaya.a31.avayaclientmobile.app;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import java.util.ArrayList;
import java.util.List;

public class FragmentScopiaInbox extends android.support.v4.app.Fragment {
    private ArrayList<NameValuePair> nameValuePairs; //Used for post request

    public List<Contact> scopiaInboxContacts;
    public int scopiaInboxCount;
    private int newCallerCount;
    public ListView scopiaInboxListView;
    MainActivity mainActivity;
    ArrayAdapter<Contact> scopiaInboxListViewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scopia_inbox, container, false);
        scopiaInboxListView = (ListView) view.findViewById(R.id.scopiaInboxListView);
        scopiaInboxListViewAdapter = new ScopiaInboxListViewAdapter(getActivity().getBaseContext());
        mainActivity = (MainActivity) getActivity();

        Button clearButton = (Button) view.findViewById(R.id.clearInboxButton);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.contactsDB.deleteAllIncomingForUser(mainActivity.userHandle);
                updateScopiaInboxListView();
            }
        });
        Button closeButton = (Button) view.findViewById(R.id.closeInboxButton);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PublicMethods.hideScopiaInbox(mainActivity);
            }
        });

        return view;
    }
    public void initScopiaInboxListView(){
        scopiaInboxContacts = new ArrayList<Contact>();
        scopiaInboxContacts = mainActivity.contactsDB.getAllIncomingForUser(mainActivity.userHandle);
        // Store contacts into database
        for (Contact newCaller:mainActivity.newCallers){
            mainActivity.contactsDB.addRecent(mainActivity.userHandle,newCaller,false,newCaller.getCallTimeStr());
        }
        scopiaInboxContacts.addAll(mainActivity.newCallers);
        //Find the max positions;
        scopiaInboxCount = scopiaInboxContacts.size()-1;
        newCallerCount = mainActivity.newCallers.size()-1;
        scopiaInboxListView.setAdapter(scopiaInboxListViewAdapter);

    }

    public void updateScopiaInboxListView(){
        scopiaInboxContacts= new ArrayList<Contact>();
        scopiaInboxContacts = mainActivity.contactsDB.getAllIncomingForUser(mainActivity.userHandle);
        //Find the max positions;
        scopiaInboxCount = scopiaInboxContacts.size()-1;
        newCallerCount = -1;
        scopiaInboxListView.setAdapter(scopiaInboxListViewAdapter);
    }
    
    private class ScopiaInboxListViewAdapter extends ArrayAdapter<Contact>{
        LayoutInflater inflater;

        public ScopiaInboxListViewAdapter(Context context){//The constructor
            super(context,R.layout.cell_scopia_inbox,scopiaInboxContacts);
            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        }

        @Override
        public int getCount() {
            return scopiaInboxContacts.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View cellView = convertView;

            if (cellView == null){
                cellView = inflater.inflate(R.layout.cell_scopia_inbox, parent, false);
            }

            final Contact contactEntry = scopiaInboxContacts.get(scopiaInboxCount-position);
            //Fill the cellView
            TextView displayName = (TextView) cellView.findViewById(R.id.scopiaInboxDisplayName);
            displayName.setText("["+contactEntry.getExtension()+"] "+contactEntry.getGivenName()+" "+contactEntry.getFamilyName());
            TextView callTime = (TextView) cellView.findViewById(R.id.scopiaInboxCallTime);
            callTime.setText(PublicMethods.dateToDisplayString(contactEntry.getCallTime()));

            Button acceptButton = (Button) cellView.findViewById(R.id.acceptScopiaButton);
            acceptButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mainActivity.openScopiaLink(contactEntry.getExtension());
                }
            });

            if(PublicMethods.compareDateWithNow(contactEntry.getCallTime())){
                acceptButton.setEnabled(true);
                acceptButton.setTextColor(Color.RED);
            }else{
                acceptButton.setEnabled(false);
                acceptButton.setTextColor(Color.GRAY);
            }

            if (position<=newCallerCount){
                displayName.setTypeface(null, Typeface.BOLD);
                acceptButton.setTypeface(null, Typeface.BOLD);
            }else{
                displayName.setTypeface(null,Typeface.NORMAL);
                acceptButton.setTypeface(null, Typeface.NORMAL);
            }
            return cellView;
        }
    }


}