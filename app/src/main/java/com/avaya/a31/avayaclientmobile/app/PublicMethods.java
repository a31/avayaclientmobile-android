package com.avaya.a31.avayaclientmobile.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import org.apache.http.conn.util.InetAddressUtils;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class PublicMethods {
    public static String validInitials = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static String[] allInitials = {"★","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"};

    public static String genValidInitial(String familyName){
        if(familyName.length()>=1){
            String initial = familyName.substring(0,1).toUpperCase();
            if(validInitials.contains(initial)){
                return initial;
            }else{
                return "#";
            }
        }else{
            return "#";
        }
    }

    public static HttpParams httpParameters(){
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, 5000); // Set time out;
        //HttpConnectionParams.setSoTimeout(httpParameters, 5000);
        return httpParameters;
    }

    public static double getVersion(Context context) {
        double v = 0.0;
        try {
            v = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return v;
    }

    public static AlertDialog makeOpenScopiaRoomAlertDialog(final MainActivity mainActivity){
        // call output.show() to display the alert dialog.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mainActivity);
        alertDialogBuilder.setMessage(R.string.enter_scopia_room_alert_message);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                mainActivity.openScopiaLink(mainActivity.userExtension);
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertDialogBuilder.setCancelable(false);
        return alertDialogBuilder.create();

    }

    public static AlertDialog makeUpdateAlertDialog(final MainActivity mainActivity){
        // call output.show() to display the alert dialog.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mainActivity);
        alertDialogBuilder.setMessage(R.string.update_alert_message);
        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                mainActivity.installUpdateApk();
            }
        });
        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertDialogBuilder.setCancelable(false);
        return alertDialogBuilder.create();

    }

    public static AlertDialog makeScopiaInvitationReceivedAlertDialog(final MainActivity mainActivity, String udpMessage){
        // ----------- Parse udpMessage -------------------
        // Format: [dt_SCOPIA_INV %s %s %s %s %s]", scopiaValue, sender, firstName, lastName, receiver
        final String[] separatedUdpMessage = udpMessage.split(" "); //
        String displayMessage = String.format("%s, %s(%s) invites you to a Scopia call.",
                separatedUdpMessage[3],separatedUdpMessage[4],separatedUdpMessage[2]);

        // call output.show() to display the alert dialog.
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mainActivity);
        alertDialogBuilder.setMessage(displayMessage);
        alertDialogBuilder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                mainActivity.setInvitePartExtension(separatedUdpMessage[2]);
                mainActivity.openScopiaLink(mainActivity.invitePartExtension);
            }
        });
        alertDialogBuilder.setNegativeButton("Decline", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
            }
        });
        alertDialogBuilder.setCancelable(false);
        return alertDialogBuilder.create();
    }

    // Date to string and string to date conversion methods
    public static Date stringToDate(String SQLStr){
        SimpleDateFormat  dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss Z");
        try {
            Date date = dateFormat.parse(SQLStr);
            return date;
        } catch (Exception exception) {
            return null;
        }
    }
    public static String dateToString(Date date){
        SimpleDateFormat  dateFormat = new SimpleDateFormat("dd MMM yyyy HH:mm:ss Z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(date);
    }
    public static String dateToDisplayString(Date date){
        SimpleDateFormat  dateFormat = new SimpleDateFormat("MMM,dd,yyyy  EEE  hh:mm a");
        dateFormat.setTimeZone(TimeZone.getDefault());
        return dateFormat.format(date);
    }

    public static void hideScopiaInbox(MainActivity mainActivity){
        Fragment fragmentScopiaInbox = mainActivity.getSupportFragmentManager().findFragmentById(R.id.fragment_scopia_inbox_fragment);
        FragmentTransaction ft= mainActivity.getSupportFragmentManager().beginTransaction();
        ft.hide(fragmentScopiaInbox).commit();
    }

    public static void showScopiaInbox(MainActivity mainActivity){
        Fragment fragmentScopiaInbox = mainActivity.getSupportFragmentManager().findFragmentById(R.id.fragment_scopia_inbox_fragment);
        FragmentTransaction ft= mainActivity.getSupportFragmentManager().beginTransaction();
        ft.show(fragmentScopiaInbox).commit();
    }

    public static String getIPAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress().toUpperCase();
                        boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 port suffix
                                return delim<0 ? sAddr : sAddr.substring(0, delim);
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    /**
     * This function compares input time with current time and returns true if input time is with in the past 1 hr
     * @param date A java util class that stores time
     * @return a boolean. If current time is more than 1hr ahead of input time return false
     */
    public static boolean compareDateWithNow(Date date){
        Calendar nowCalendar = Calendar.getInstance();
        Calendar dateCalendar = Calendar.getInstance();
        dateCalendar.setTime(date);
        long milliSecNow = nowCalendar.getTimeInMillis();
        long milliSecDate = dateCalendar.getTimeInMillis();
        if (milliSecNow-milliSecDate>3600000){
            return false;
        }else{return true;}
    }

}
