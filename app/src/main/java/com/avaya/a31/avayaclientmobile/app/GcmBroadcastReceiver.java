package com.avaya.a31.avayaclientmobile.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import java.util.ArrayList;

/**
 * Created by a31 on 7/16/2014.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {
    public String registrationID;
    private Context systemContext;
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("com.google.android.c2dm.intent.REGISTRATION")){
            String registrationId = intent.getStringExtra("registration_id");
            if (registrationId!=null){
                registrationID = registrationId;
                systemContext = context;
                //Log.d("OOOOOOOOO RegistrationID is:",registrationId);
                new PostDeviceIPAsyncTask().execute();
            }
            //String error = intent.getStringExtra("error");
            //String unregistered = intent.getStringExtra("unregistered");
        }else if(action.equals("com.google.android.c2dm.intent.RECEIVE")){

            // Explicitly specify that GcmMessageHandler will handle the intent.
            ComponentName comp = new ComponentName(context.getPackageName(),
                    GcmMessageHandler.class.getName());

            // Start the service, keeping the device awake while it is launching.
            startWakefulService(context, (intent.setComponent(comp)));
            setResultCode(Activity.RESULT_OK);
        }
    }

    public class PostDeviceIPAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                SharedPreferences userInfo= systemContext.getSharedPreferences(systemContext.getString(R.string.user_info), Context.MODE_PRIVATE);
                String userExtension = userInfo.getString("userExtension", "0"); //Return a single zeros by default;
                ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("extension",userExtension));
                nameValuePairs.add(new BasicNameValuePair("device_token",registrationID));
                nameValuePairs.add(new BasicNameValuePair("device_ip",PublicMethods.getIPAddress(true)));
                nameValuePairs.add(new BasicNameValuePair("token","'null'"));
                nameValuePairs.add(new BasicNameValuePair("device_type","android"));
                nameValuePairs.add(new BasicNameValuePair("device_ssl_string","'null'"));

                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpPost httpPost = new HttpPost(systemContext.getString(R.string.device_ip_token_url));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                /*
                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);}
                }*/
            }catch(Exception exception){
                exception.printStackTrace();
                Toast.makeText(systemContext, R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
            return null;
        }
    }
}
