package com.avaya.a31.avayaclientmobile.app;
import java.util.Date;

/** Contact custom class that can be used for list views.
 * Created by a31 on 7/1/2014.
 * Contact class does not store initial information. this is only for
 * displaying content and referencing to the database with _id
 */
public class Contact {// Does not need to store initial
    //Declare private properties
    private long _id; //The temporary id from the database
    private String handle;
    private String extension;
    private String givenName;
    private String familyName;
    private String location;
    private String email;
    private String displayName;
    private boolean favorite;
    private boolean checked;

    private boolean direction; // 0 means incoming 1 means outgoing,

    //Future attributes for recent calls
    private Date callTime;
    private boolean scheduleTime;
    // this variable is only used for displaying recent call content

    // empty constructor
    public Contact(){}
    // Constructor with all fields(favorite is set to false for default)
    //Contact class constructor for local display selection enabled.
    public Contact(long id, String handle, String extension, String givenName, String familyName, String location, String email, boolean favorite) {
        this._id = id;
        this.handle = handle;
        this.extension = extension;
        this.givenName = givenName;
        this.familyName = familyName;
        this.location = location;
        this.email = email;
        this.displayName = familyName.concat(", ").concat(givenName);
        this.favorite = favorite;
        this.checked= false;
    }
    // This is the constructor for recent calls.
    public Contact(long id, String handle, String extension, String givenName, String familyName, String location, String email, boolean direction, String callTimeStr) {
        this._id = id;
        this.handle = handle;
        this.extension = extension;
        this.givenName = givenName;
        this.familyName = familyName;
        this.location = location;
        this.email = email;
        this.displayName = familyName.concat(", ").concat(givenName);
        this.direction = direction;
        this.callTime = PublicMethods.stringToDate(callTimeStr);
    }

    //Contact class constructor for LDAP search
    public Contact(String handle, String extension, String givenName, String familyName, String location, String email) {
        this.handle = handle;
        this.extension = extension;
        this.givenName = givenName;
        this.familyName = familyName;
        this.location = location;
        this.email = email;
        this.displayName = familyName.concat(", ").concat(givenName);
    }
    //Contact class constructor for Scopia Inbox
    public Contact(String handle, String extension, String givenName, String familyName, String location, String email, String callTimeStr) {
        this.handle = handle;
        this.extension = extension;
        this.givenName = givenName;
        this.familyName = familyName;
        this.location = location;
        this.email = email;
        this.displayName = familyName.concat(", ").concat(givenName);
        this.callTime = PublicMethods.stringToDate(callTimeStr);
    }

    public String getInitial(){
        return PublicMethods.genValidInitial(familyName);
    }

    public String getHandle() {
        if(handle == null){
            return "";
        }else{
            return handle;
        }
    }

    public void setHandle(String handle) {
        this.handle = handle;
    }

    public String getExtension() {
        if(extension == null){
            return "";
        }else{
            return extension;
        }
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getGivenName() {
        if(givenName == null){
            return "";
        }else{
            return givenName;
        }
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        if(familyName == null){
            return "";
        }else{
            return familyName;
        }
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getLocation() {
        if(location == null){
            return "";
        }else{
            return location;
        }
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmail() {
        if(email == null){
            return "";
        }else{
            return email;
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDisplayName() {
        if(displayName == null){
            return "";
        }else{
            return displayName;
        }
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isDirection() {
        return direction;
    }

    public void setDirection(boolean direction) {
        this.direction = direction;
    }

    public Date getCallTime() {
        return callTime;
    }
    public String getCallTimeStr() {
        return PublicMethods.dateToString(callTime);
    }

    public void setCallTime(Date callTime) {
        this.callTime = callTime;
    }
}
