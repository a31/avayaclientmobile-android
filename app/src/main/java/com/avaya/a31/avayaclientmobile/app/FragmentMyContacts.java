package com.avaya.a31.avayaclientmobile.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FragmentMyContacts extends Fragment implements SearchView.OnQueryTextListener{
    private SearchView searchView;
    private ListView contactsSearchListView;
    private MenuItem contactsSearchItem;
    private ArrayList<NameValuePair> nameValuePairs; //Used for post request
    private List<Contact> searchResults;
    private Map<String, List<Contact>> allContacts;
    private Object[] allKeys;
    ExpandableListView contactsListView;
    private ArrayList<String> recipients;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle saveInstanceState){
        View view = inflater.inflate(R.layout.fragment_my_contacts, container, false);
        contactsListView = (ExpandableListView)view.findViewById(R.id.contactsExpandableListView);
        contactsSearchListView = (ListView) view.findViewById(R.id.contactsSearchListView);

        // -------------- Keyboard logic -----------------
        contactsListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(contactsListView.getWindowToken(),0);
                contactsSearchListView.setVisibility(View.GONE);
                MainActivity mainActivity = (MainActivity) getActivity();
                PublicMethods.hideScopiaInbox(mainActivity);
                return false;
            }
        });// -------------- Keyboard logic ---------------

        registerForContextMenu(contactsListView);
        initExpandableView();
        return view;
    }

    public void initExpandableView(){
        getAllContacts();
        contactsListView.setAdapter(new ContactsListViewAdapter(getActivity().getBaseContext()));
        // Expand all initials by default
        for(int i=0; i<allKeys.length; i++){
            contactsListView.expandGroup(i);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.my_contacts_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        contactsSearchItem = menu.findItem(R.id.search_action_contacts);
        searchView = (SearchView) contactsSearchItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.send_scopia_invitation) {
            MainActivity mainActivity = (MainActivity) getActivity();
            if(mainActivity.scopiaRecipients.size()>0){
                mainActivity.makeScopiaRequest(mainActivity.scopiaRecipients);
            }else{
                Toast.makeText(getActivity().getBaseContext(), R.string.no_contact_selected, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean getAllContacts(){
        MainActivity mainActivity = (MainActivity) getActivity();
        allContacts=mainActivity.contactsDB.getAllContactsForUser(mainActivity.userHandle);
        allKeys = allContacts.keySet().toArray();
        if (allContacts.size()==0){
            mainActivity.actionBar.setSelectedNavigationItem(2);
        }
        return true;
    }

    public boolean onQueryTextSubmit(String searchStr) {
        searchResults = new ArrayList<Contact>();
        //Hide the keyboard
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(),0);
        if(searchStr.length()>0){ // makesure The search string is at least two characters
            contactsSearchListView.setVisibility(View.VISIBLE);
            searchResults = new ArrayList<Contact>();
            for (Object key:allKeys){
                String keyStr = key.toString();
                if (!keyStr.equals("0")){
                    List<Contact> contactsGroup = allContacts.get(keyStr);
                    for (Contact contact:contactsGroup){
                        if (contact.getEmail().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getExtension().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getLocation().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getFamilyName().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getGivenName().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }
                    }
                }
            }
            if (searchResults.size()==0){
                Toast.makeText(getActivity().getBaseContext(), R.string.no_result_found, Toast.LENGTH_SHORT).show();
            }
        }
        contactsSearchListView.setAdapter(new ContactsSearchListViewAdapter(getActivity().getBaseContext()));
        return true;
    }
    public boolean onQueryTextChange(String searchStr){
        searchResults = new ArrayList<Contact>();
        if(searchStr.length()>0){ // makesure The search string is at least two characters
            contactsSearchListView.setVisibility(View.VISIBLE);
            for (Object key:allKeys){
                String keyStr = key.toString();
                if (!keyStr.equals("0")){
                    List<Contact> contactsGroup = allContacts.get(keyStr);
                    for (Contact contact:contactsGroup){
                        if (contact.getEmail().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getExtension().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getLocation().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getFamilyName().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }else if(contact.getGivenName().toLowerCase().contains(searchStr.toLowerCase())){
                            searchResults.add(contact);
                        }
                    }
                }
            }
            if (searchResults.size()==0){
                Toast.makeText(getActivity().getBaseContext(), R.string.no_result_found, Toast.LENGTH_SHORT).show();
            }
        }
        contactsSearchListView.setAdapter(new ContactsSearchListViewAdapter(getActivity().getBaseContext()));
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        ExpandableListView.ExpandableListContextMenuInfo info =
                (ExpandableListView.ExpandableListContextMenuInfo) menuInfo;
        int type =
                ExpandableListView.getPackedPositionType(info.packedPosition);
        if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD){
            MenuInflater m = getActivity().getMenuInflater();
            m.inflate(R.menu.contact_context_menu, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.delete_contact:
                ExpandableListView.ExpandableListContextMenuInfo info = (ExpandableListView.ExpandableListContextMenuInfo) item
                        .getMenuInfo();
                int type = ExpandableListView.getPackedPositionType(info.packedPosition);
                int groupPosition = ExpandableListView.getPackedPositionGroup(info.packedPosition);
                int childPosition = ExpandableListView.getPackedPositionChild(info.packedPosition);
                long deleteID = allContacts.get(allKeys[groupPosition].toString()).get(childPosition).get_id();
                if (type == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.contactsDB.deleteRowContacts(deleteID);
                    initExpandableView();
                }
                return true;
        }
        return super.onContextItemSelected(item);
    }

    public class ContactsListViewAdapter extends BaseExpandableListAdapter {
        private Context context;
        private LayoutInflater inflater;
        private MainActivity mainActivity = (MainActivity) getActivity();

        public ContactsListViewAdapter(Context context) {
            this.context = context;
            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        }

        @Override
        public int getGroupCount() {
            return allKeys.length;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return allContacts.get(allKeys[groupPosition].toString()).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            //Object[] allKeys = allContacts.keySet().toArray();
            //Arrays.sort(allKeys);
            //return (List) allContacts.get(allKeys[groupPosition]);
            return null;
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            //Object[] allKeys = allContacts.keySet().toArray();
            //Arrays.sort(allKeys);
            //return ((List) allContacts.get(allKeys[groupPosition])).get(childPosition);
            return null;
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false; // Not known;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View cellView = convertView;
            if (cellView == null){
                cellView = inflater.inflate(R.layout.cell_contact_group, parent, false);
            }
            String initial = allKeys[groupPosition].toString();

            TextView groupTitle = (TextView) cellView.findViewById(R.id.group_title_text);
            if (initial.equals("0")){
                groupTitle.setText(getString(R.string.title_favorite));
            }else if(initial.equals("ZZ")){
                groupTitle.setText(getString(R.string.title_unknown_initial));
            }else{
                groupTitle.setText(initial);
            }

            return cellView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View cellView = convertView;
            if (cellView == null){
                cellView = inflater.inflate(R.layout.cell_contacts, parent, false);
            }
            //Locate contact entry
            final Contact contactEntry = allContacts.get(allKeys[groupPosition].toString()).get(childPosition);

            // Fill the content cell
            TextView displayName = (TextView) cellView.findViewById(R.id.displayNameLabelLocal);
            displayName.setText(contactEntry.getDisplayName());
            TextView location = (TextView) cellView.findViewById(R.id.locationLabelLocal);
            location.setText(contactEntry.getLocation());

            Button callButton = (Button) cellView.findViewById(R.id.callButtonLocal);
            callButton.setText(contactEntry.getExtension());
            Button emailButton = (Button) cellView.findViewById(R.id.emailButtonLocal);
            emailButton.setText(contactEntry.getEmail());

            ImageButton addFavoriteButton = (ImageButton) cellView.findViewById(R.id.addFavoriteButtonLocal);
            if (contactEntry.isFavorite()){
                addFavoriteButton.setImageResource(R.drawable.btn_rating_star_on);
            }else{
                addFavoriteButton.setImageResource(R.drawable.btn_rating_star_off);
            }

            // Set onClick listeners
            addFavoriteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mainActivity.contactsDB.updateFavorite(contactEntry.get_id(),!contactEntry.isFavorite());
                    initExpandableView();
                }
            });
            ImageButton callIconButton = (ImageButton) cellView.findViewById(R.id.callIconButtonLocal);
            ImageButton emailIconButton = (ImageButton) cellView.findViewById(R.id.emailIconButtonLocal);

            callButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(IntentMaker.makeCallIntent(contactEntry.getExtension()));
                }
            });
            emailButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(IntentMaker.makeEmailIntent(contactEntry.getEmail()));
                }
            });

            //callIconButton.setOnClickListener(callButtonListener);
            //emailIconButton.setOnClickListener(emailButtonListener);
            ImageButton scopiaButton = (ImageButton) cellView.findViewById(R.id.scopiaButtonLocal);
            scopiaButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ArrayList<Contact> singleRecipient = new ArrayList<Contact>();
                    singleRecipient.add(contactEntry);
                    mainActivity.makeScopiaRequest(singleRecipient);
                }
            });
            final CheckBox checkButton = (CheckBox) cellView.findViewById(R.id.checkButton);
            if (mainActivity.scopiaRecipients.contains(contactEntry)){
                checkButton.setChecked(true);
            }else{
                checkButton.setChecked(false);
            }
            checkButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkButton.isChecked()){
                        mainActivity.addRecipient(contactEntry);
                    }else{
                        mainActivity.removeRecipient(contactEntry);
                    }
                }
            });
            // *********This line ensures long clickable for deleting function**********
            cellView.setLongClickable(true);
            return cellView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {return false;}

    }

    private class ContactsSearchListViewAdapter extends ArrayAdapter<Contact> {
        LayoutInflater inflater;
        private MainActivity mainActivity = (MainActivity) getActivity();

        public ContactsSearchListViewAdapter(Context context){//The constructor
            super(context,R.layout.cell_ldap_search,searchResults);
            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        }

        @Override
        public int getCount() {
            if (searchResults.size()>0){
                return searchResults.size()+1;
            }else{
                return 0;
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View cellView = convertView;
            if (position<searchResults.size()){
                if (cellView == null){
                    cellView = inflater.inflate(R.layout.cell_contacts, parent, false);
                }

                //Get the content from storage
                final Contact contactSearchEntry = searchResults.get(position);
                // Fill the content cell
                TextView displayName = (TextView) cellView.findViewById(R.id.displayNameLabelLocal);
                displayName.setText(contactSearchEntry.getDisplayName());
                TextView location = (TextView) cellView.findViewById(R.id.locationLabelLocal);
                location.setText(contactSearchEntry.getLocation());

                Button callButton = (Button) cellView.findViewById(R.id.callButtonLocal);
                callButton.setText(contactSearchEntry.getExtension());
                Button emailButton = (Button) cellView.findViewById(R.id.emailButtonLocal);
                emailButton.setText(contactSearchEntry.getEmail());

                final ImageButton searchAddFavoriteButton = (ImageButton) cellView.findViewById(R.id.addFavoriteButtonLocal);
                if (contactSearchEntry.isFavorite()){
                    searchAddFavoriteButton.setImageResource(R.drawable.btn_rating_star_on);
                }else{
                    searchAddFavoriteButton.setImageResource(R.drawable.btn_rating_star_off);
                }

                // Set onClick listeners
                searchAddFavoriteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mainActivity.contactsDB.updateFavorite(contactSearchEntry.get_id(),!contactSearchEntry.isFavorite());
                        contactSearchEntry.setFavorite(!contactSearchEntry.isFavorite());
                        if (contactSearchEntry.isFavorite()){
                            searchAddFavoriteButton.setImageResource(R.drawable.btn_rating_star_on);
                        }else{
                            searchAddFavoriteButton.setImageResource(R.drawable.btn_rating_star_off);
                        }
                        initExpandableView();
                    }
                });
                ImageButton callIconButton = (ImageButton) cellView.findViewById(R.id.callIconButtonLocal);
                ImageButton emailIconButton = (ImageButton) cellView.findViewById(R.id.emailIconButtonLocal);

                callButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(IntentMaker.makeCallIntent(contactSearchEntry.getExtension()));
                    }
                });
                emailButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(IntentMaker.makeEmailIntent(contactSearchEntry.getEmail()));
                    }
                });

                //callIconButton.setOnClickListener(callButtonListener);
                //emailIconButton.setOnClickListener(emailButtonListener);
                ImageButton scopiaButton = (ImageButton) cellView.findViewById(R.id.scopiaButtonLocal);
                scopiaButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ArrayList<Contact> singleRecipient = new ArrayList<Contact>();
                        singleRecipient.add(contactSearchEntry);
                        mainActivity.makeScopiaRequest(singleRecipient);
                    }
                });
                final CheckBox checkButton = (CheckBox) cellView.findViewById(R.id.checkButton);
                if (mainActivity.scopiaRecipients.contains(contactSearchEntry)){
                    checkButton.setChecked(true);
                }else{
                    checkButton.setChecked(false);
                }
                checkButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (checkButton.isChecked()){
                            mainActivity.addRecipient(contactSearchEntry);
                        }else{
                            mainActivity.removeRecipient(contactSearchEntry);
                        }
                    }
                });
                // *********This line ensures long clickable for deleting function**********
                cellView.setLongClickable(true);
                cellView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        //TODO: Enable long click to delete from search
                        return false;
                    }
                });
            }else{
                if (cellView == null){
                    cellView = inflater.inflate(R.layout.cell_contact_border, parent, false);
                }
            }

            return cellView;
        }

    }
}
