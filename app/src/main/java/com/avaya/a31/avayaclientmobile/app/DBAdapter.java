package com.avaya.a31.avayaclientmobile.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DBAdapter{
    /////////////////////////////////////////////////////////////////////
    //	Constants & Data
    /////////////////////////////////////////////////////////////////////
    private static final String TAG = "DBAdapter";
    public  Context context;
    public DatabaseHelper myDBHelper;
    public SQLiteDatabase db;

    // DB info: it's name, and the table we are using (just one).
    public static final String DATABASE_NAME = "AvayaDB.db";
    public static final int DATABASE_VERSION = 1;

    // DB Fields
    public static final String KEY_ROWID = "_id";
    public static final String KEY_USER = "User";
    public static final String KEY_HANDLE = "Handle";
    public static final String KEY_EXTENSION = "Extension";
    public static final String KEY_GIVENNAME = "GivenName";
    public static final String KEY_FAMILYNAME = "FamilyName";
    public static final String KEY_LOCATION = "Location";
    public static final String KEY_EMAIL = "Email";
    public static final String KEY_INITIAL = "Initial";
    public static final String KEY_FAVORITE = "Favorite";       // 0 for not-favorited
    public static final String KEY_DIRECTION = "Direction";      // 1 for out going call.
    public static final String KEY_CALLTIME = "Calltime";


    // Field column numbers here
    public static final int COL_ROWID = 0;
    public static final int COL_USER = 1;
    public static final int COL_HANDLE = 2;
    public static final int COL_EXTENSION = 3;
    public static final int COL_GIVENNAME = 4;
    public static final int COL_FAMILYNAME = 5;
    public static final int COL_LOCATION = 6;
    public static final int COL_EMAIL = 7;
    public static final int COL_INITIAL = 8;
    public static final int COL_FAVORITE = 9;
    public static final int COL_DIRECTION = 8;
    public static final int COL_CALLTIME = 9;

    public static final String[] ALL_KEYS_CONTACTS = new String[] {KEY_ROWID, KEY_USER, KEY_HANDLE, KEY_EXTENSION, KEY_GIVENNAME, KEY_FAMILYNAME,KEY_LOCATION,KEY_EMAIL,KEY_INITIAL,KEY_FAVORITE};
    public static final String[] ALL_KEYS_RECENT = new String[] {KEY_ROWID, KEY_USER, KEY_HANDLE, KEY_EXTENSION, KEY_GIVENNAME, KEY_FAMILYNAME,KEY_LOCATION,KEY_EMAIL,KEY_DIRECTION,KEY_CALLTIME};

    private static String DATABASE_TABLE_CONTACTS = "ContactsTable";
    private static String DATABASE_TABLE_RECENT = "RecentTable";
    private static String DATABASE_CREATE_CONTACTS =
        "create table " + DATABASE_TABLE_CONTACTS
        + " (" + KEY_ROWID + " integer primary key autoincrement, "
        + KEY_USER + " text not null, "
        + KEY_HANDLE + " text not null, "
        + KEY_EXTENSION + " text not null, "
        + KEY_GIVENNAME + " text not null, "
        + KEY_FAMILYNAME + " text not null, "
        + KEY_LOCATION + " text not null, "
        + KEY_EMAIL + " text not null, "
        + KEY_INITIAL + " text not null, "
        + KEY_FAVORITE + " integer not null"
        + ");";

    private static String DATABASE_CREATE_RECENT =
        "create table " + DATABASE_TABLE_RECENT
        + " (" + KEY_ROWID + " integer primary key autoincrement, "
        + KEY_USER + " text not null, "
        + KEY_HANDLE + " text not null, "
        + KEY_EXTENSION + " text not null, "
        + KEY_GIVENNAME + " text not null, "
        + KEY_FAMILYNAME + " text not null, "
        + KEY_LOCATION + " text not null, "
        + KEY_EMAIL + " text not null, "
        + KEY_DIRECTION + " text not null, "
        + KEY_CALLTIME + " text not null"
        + ");";

    /////////////////////////////////////////////////////////////////////
    //	Public methods:
    /////////////////////////////////////////////////////////////////////

    public DBAdapter(Context ctx) {
        this.context = ctx;
        myDBHelper = new DatabaseHelper(context);
    }

    // Open the database connection.
    public DBAdapter open() {
        db = myDBHelper.getWritableDatabase();
        return this;
    }

    // Close the database connection.
    public void close() {
        myDBHelper.close();
    }

    // Add a new set of values to the database.
    public long insertRowContacts(String user, String handle, String extension, String givenName, String familyName, String location, String email){
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_USER, user);
        initialValues.put(KEY_HANDLE, handle);
        initialValues.put(KEY_EXTENSION, extension);
        initialValues.put(KEY_GIVENNAME, givenName);
        initialValues.put(KEY_FAMILYNAME, familyName);
        initialValues.put(KEY_LOCATION, location);
        initialValues.put(KEY_EMAIL, email);
        initialValues.put(KEY_INITIAL, PublicMethods.genValidInitial(familyName));
        initialValues.put(KEY_FAVORITE, 0); //Set not favorited when data first added.
        // Insert it into the database.
        return db.insert(DATABASE_TABLE_CONTACTS, null, initialValues);
    }

    /////////////////////////////////////////////////////////////////////
    //	Custom methods:
    /////////////////////////////////////////////////////////////////////
    public Contact getContactWithHandle(String user, String handle){
        String query = "Select * FROM "+DATABASE_TABLE_CONTACTS+" WHERE "+KEY_USER+" LIKE ? AND "+KEY_HANDLE+" LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user,handle});
        if (c.moveToFirst()) {
            Contact contact =
                    new Contact(c.getInt(COL_ROWID),c.getString(COL_HANDLE),c.getString(COL_EXTENSION),c.getString(COL_GIVENNAME),
                            c.getString(COL_FAMILYNAME),c.getString(COL_LOCATION),c.getString(COL_EMAIL),false);
            c.close();
            return contact;
        }else{
            return null;
        }
    }
    public Contact getContactWithExtension(String user, String extension){
        String query = "Select * FROM "+DATABASE_TABLE_CONTACTS+" WHERE "+KEY_USER+" LIKE ? AND "+KEY_EXTENSION+" LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user,extension});
        if (c.moveToFirst()) {
            Contact contact =
                    new Contact(c.getInt(COL_ROWID),c.getString(COL_HANDLE),c.getString(COL_EXTENSION),c.getString(COL_GIVENNAME),
                            c.getString(COL_FAMILYNAME),c.getString(COL_LOCATION),c.getString(COL_EMAIL),false);
            c.close();
            return contact;
        }else{
            return null;
        }
    }

    public boolean addContact(String user, Contact contact){
        String query = "Select "+KEY_ROWID+","+KEY_FAVORITE+" FROM "+DATABASE_TABLE_CONTACTS+" WHERE "+KEY_USER+" LIKE ? AND "+KEY_HANDLE+" LIKE ?;";
        //String query = "Select * FROM "+DATABASE_TABLE_CONTACTS+" WHERE "+KEY_USER+" LIKE ? AND "+KEY_HANDLE+" LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user,contact.getHandle()});
        ContentValues values = new ContentValues();
        values.put(KEY_USER, user);
        values.put(KEY_HANDLE, contact.getHandle());
        values.put(KEY_EXTENSION, contact.getExtension());
        values.put(KEY_GIVENNAME, contact.getGivenName());
        values.put(KEY_FAMILYNAME, contact.getFamilyName());
        values.put(KEY_LOCATION, contact.getLocation());
        values.put(KEY_EMAIL, contact.getEmail());
        values.put(KEY_INITIAL, PublicMethods.genValidInitial(contact.getFamilyName()));
        int favStat = 0;
        if (c.moveToFirst()) {
            do {
                favStat = c.getInt(1);  //#1 is the column for favorite status(0:id,1:fav)
                String where = KEY_ROWID + " = " + c.getInt(COL_ROWID);
                values.put(KEY_FAVORITE,favStat);
                db.update(DATABASE_TABLE_CONTACTS, values, where, null);
            } while (c.moveToNext());
            c.close();
            return false;// Returns 0 if this is an update

        }else{ // Add a new entry
            values.put(KEY_FAVORITE, favStat); //Set not favorited when data first added.
            long newID = db.insert(DATABASE_TABLE_CONTACTS, null, values);// This returns the added _id
            return true; //return 1 if this is a new entry
        }
    }

    public Map<String, List<Contact>> getAllContactsForUser(String user){
        //Map<String, List<Contact>> contactsMap = new HashMap<String, List<Contact>>();
        Map<String, List<Contact>> contactsMap = new TreeMap();
        //List<Contact> contactList = new ArrayList<Contact>();
        String query = "Select * FROM " + DATABASE_TABLE_CONTACTS + " WHERE " + KEY_USER + " LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user});
        if (c.moveToFirst()) {
            do {
                // Read the favorite status
                Contact contact =
                        new Contact(c.getInt(COL_ROWID),c.getString(COL_HANDLE),c.getString(COL_EXTENSION),c.getString(COL_GIVENNAME),
                                c.getString(COL_FAMILYNAME),c.getString(COL_LOCATION),c.getString(COL_EMAIL),false);
                if(c.getInt(COL_FAVORITE)==1){
                    //favStats = true;
                    contact.setFavorite(true);
                    List<Contact> favoriteGroupList = contactsMap.get("0");  //Char "0" ★☆ means favorite
                    if(favoriteGroupList == null) {
                        favoriteGroupList = new ArrayList<Contact>();
                        favoriteGroupList.add(contact);
                        contactsMap.put("0", favoriteGroupList);
                    } else {
                        // add if item is not already in list
                        favoriteGroupList.add(contact);
                    }
                }
                if (PublicMethods.validInitials.contains(c.getString(COL_INITIAL))){
                    // Adding contact to list
                    List<Contact> groupList = contactsMap.get(c.getString(COL_INITIAL));
                    if(groupList == null) {
                        groupList = new ArrayList<Contact>();
                        groupList.add(contact);
                        contactsMap.put(c.getString(COL_INITIAL), groupList);
                    } else {
                        // add if item is not already in list
                        groupList.add(contact);
                    }
                }else{
                    List<Contact> groupList = contactsMap.get("ZZ"); //ZZ means other initials
                    if(groupList == null) {
                        groupList = new ArrayList<Contact>();
                        groupList.add(contact);
                        contactsMap.put("ZZ", groupList);
                    } else {
                        // add if item is not already in list
                        groupList.add(contact);
                    }
                }
                //contactList.add(contact);
            } while (c.moveToNext());
            c.close();
        }
        return contactsMap;
    }

    public List<Contact> getAllRecentsForUser(String user){
        List<Contact> recentList = new ArrayList<Contact>();
        String query = "Select * FROM " + DATABASE_TABLE_RECENT + " WHERE " + KEY_USER + " LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user});
        if (c.moveToFirst()) {
            do {
                boolean direction = false;
                if(c.getString(COL_DIRECTION).equals("out")){
                    direction = true;
                }
                Contact contact =
                        new Contact(c.getInt(COL_ROWID),c.getString(COL_HANDLE),c.getString(COL_EXTENSION),c.getString(COL_GIVENNAME),
                                c.getString(COL_FAMILYNAME),c.getString(COL_LOCATION),c.getString(COL_EMAIL),direction,c.getString(COL_CALLTIME));
                // Adding contact to list
                recentList.add(contact);
            } while (c.moveToNext());
            c.close();
        }
        return recentList;
    }

    public List<Contact> getAllIncomingForUser(String user){
        List<Contact> scopiaInboxList = new ArrayList<Contact>();
        String query = "Select * FROM " + DATABASE_TABLE_RECENT + " WHERE " + KEY_USER + " LIKE ? and "+ KEY_DIRECTION + " LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user,"in"});
        if (c.moveToFirst()) {
            do {
                boolean direction = false;
                Contact contact =
                        new Contact(c.getInt(COL_ROWID),c.getString(COL_HANDLE),c.getString(COL_EXTENSION),c.getString(COL_GIVENNAME),
                                c.getString(COL_FAMILYNAME),c.getString(COL_LOCATION),c.getString(COL_EMAIL),direction,c.getString(COL_CALLTIME));
                // Adding contact to list
                scopiaInboxList.add(contact);
            } while (c.moveToNext());
            c.close();
        }
        return scopiaInboxList;
    }

    public long updateFavorite(long rowId, boolean favStats){
        String where = KEY_ROWID + "=" + rowId;
        ContentValues newValues = new ContentValues();
        //TODO: This is a light weight update, check if this is true!
        if(favStats){
            newValues.put(KEY_FAVORITE, 1);
        }else{
            newValues.put(KEY_FAVORITE, 0);
        }
        return db.update(DATABASE_TABLE_CONTACTS, newValues, where, null);
    }

    // Delete a row from the database, by rowId (primary key)
    public boolean deleteRowContacts(long rowId) {
        String where = KEY_ROWID + "=" + rowId;
        return db.delete(DATABASE_TABLE_CONTACTS, where, null) != 0;
    }

    public boolean addRecent(String user, Contact contact, boolean direction, String dateStr){
        ContentValues values = new ContentValues();
        values.put(KEY_USER, user);
        values.put(KEY_HANDLE, contact.getHandle());
        values.put(KEY_EXTENSION, contact.getExtension());
        values.put(KEY_GIVENNAME, contact.getGivenName());
        values.put(KEY_FAMILYNAME, contact.getFamilyName());
        values.put(KEY_LOCATION, contact.getLocation());
        values.put(KEY_EMAIL, contact.getEmail());
        if(direction){
            values.put(KEY_DIRECTION, "out");
        }else{
            values.put(KEY_DIRECTION, "in");
        }
        values.put(KEY_CALLTIME,dateStr);
        // Insert it into the database.
        long newID = db.insert(DATABASE_TABLE_RECENT, null, values);// This returns the added _id
        //return db.insert(DATABASE_TABLE_RECENT, null, values) != 0 ;
        return true;
    }

    public boolean deleteRowRecent(long rowId) {
        String where = KEY_ROWID + "=" + rowId;
        return db.delete(DATABASE_TABLE_RECENT, where, null) != 0;
    }

    public void deleteAllRecentForUser(String user){
        String query = "Select "+KEY_ROWID+" FROM " + DATABASE_TABLE_RECENT + " WHERE " + KEY_USER + " LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user});
        long rowId = c.getColumnIndexOrThrow(KEY_ROWID);
        if (c.moveToFirst()) {
            do {
                deleteRowRecent(c.getLong((int) rowId));
            } while (c.moveToNext());
        }
        c.close();
    }

    public void deleteAllIncomingForUser(String user){
        String query = "Select * FROM " + DATABASE_TABLE_RECENT + " WHERE " + KEY_USER + " LIKE ? and "+ KEY_DIRECTION + " LIKE ?;";
        Cursor c = db.rawQuery(query, new String[]{user,"in"});
        long rowId = c.getColumnIndexOrThrow(KEY_ROWID);
        if (c.moveToFirst()) {
            do {
                deleteRowRecent(c.getLong((int) rowId));
            } while (c.moveToNext());
        }
        c.close();
    }

    /////////////////////////////////////////////////////////////////////
    //	Private Helper Classes:
    /////////////////////////////////////////////////////////////////////

    public class DatabaseHelper extends SQLiteOpenHelper
    {
        //private String userHandle;

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase _db) {
            try{
                _db.execSQL(DATABASE_CREATE_CONTACTS);
                _db.execSQL(DATABASE_CREATE_RECENT);
            }catch(Exception exception){}
        }

        @Override
        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading application's database from version " + oldVersion
                    + " to " + newVersion + ", which will destroy all old data!");
            // Destroy old database:
            _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CONTACTS);
            _db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_RECENT);
            // Recreate new database:
            onCreate(_db);
        }

    }
}
