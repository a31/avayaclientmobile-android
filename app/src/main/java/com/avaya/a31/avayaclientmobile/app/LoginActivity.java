package com.avaya.a31.avayaclientmobile.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends Activity implements View.OnClickListener{
    EditText username, password; // Edit text box
    Button loginButton;
    ArrayList<NameValuePair> nameValuePairs;
    private ProgressBar spinner;
    boolean loginSuccess;
    boolean connectionSuccess = false;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences userInfo = getSharedPreferences(getString(R.string.user_info),Context.MODE_PRIVATE);
        loginSuccess = userInfo.getBoolean("loginCheck", false); // Default value for loginSuccess is false

        if(loginSuccess){
            Intent mainActivityIntent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(mainActivityIntent);
            finish();
        }else{
            init();
        }


    }
    private void init(){
        username = (EditText) findViewById(R.id.globalHandleTextField);
        password = (EditText) findViewById(R.id.passwordTextField);
        loginButton = (Button) findViewById(R.id.loginButton);
        spinner = (ProgressBar) findViewById(R.id.loginProgressBar);
        spinner.setVisibility(View.GONE);
        //Set an onClick listener
        loginButton.setOnClickListener(this);

        (new ContactValidatorFactory<Activity>()).setUpValidators(this);
    }
    @Override
    public void onClick(View view){//socpia:
        switch(view.getId()){
            case R.id.loginButton:
                // Hide the keyboard first
                InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(username.getWindowToken(),0);

                String usernameStr = username.getText().toString();
                String passwordStr = password.getText().toString();

                if(usernameStr.length()<2) {
                    Toast.makeText(getBaseContext(), R.string.username_tooshort, Toast.LENGTH_SHORT).show();
                }else if(passwordStr.length()<5){
                    Toast.makeText(getBaseContext(),R.string.password_tooshort,Toast.LENGTH_SHORT).show();
                }else{
                    spinner.setVisibility(View.VISIBLE);
                    nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("username",usernameStr));
                    nameValuePairs.add(new BasicNameValuePair("password",passwordStr));
                    new LoginAsyncTask().execute();
                }
                break;
        }
    }
    public class LoginAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        @Override
        protected Void doInBackground(Void... params){
            try{
                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpPost httpPost = new HttpPost(getString(R.string.ldap_auth_url));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs,HTTP.UTF_8));
                HttpResponse httpResponse = httpClient.execute(httpPost);
                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    connectionSuccess= true;
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);
                        //String authStatus = jsonResponse.getString("status");
                        if (username.getText().toString().equals(jsonResponse.getString("handle"))&&jsonResponse.getInt("status")==0){
                            //Write all user information in before entering the app
                            SharedPreferences sp = getSharedPreferences(getString(R.string.user_info),Context.MODE_PRIVATE);
                            SharedPreferences.Editor spEdit = sp.edit();
                            spEdit.clear();
                            spEdit.putBoolean("loginCheck",true);
                            spEdit.putString("userHandle",jsonResponse.getString("handle"));
                            spEdit.putString("userFirstName",jsonResponse.getString("firstName"));
                            spEdit.putString("userLastName",jsonResponse.getString("lastName"));
                            spEdit.putString("userEmail",jsonResponse.getString("email"));
                            spEdit.putString("userExtension",jsonResponse.getString("extension"));
                            spEdit.commit();

                            loginSuccess = true;
                        }
                    }
                }
            }catch(Exception exception){
                exception.printStackTrace();
                connectionSuccess = false;
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void unused){
            spinner.setVisibility(View.GONE);
            if (loginSuccess){

                Toast.makeText(getBaseContext(),R.string.login_succeed,Toast.LENGTH_SHORT).show();

                // Switch to the main view
                Intent mainActivityIntent = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(mainActivityIntent);
                finish();
            }else{
                // Pop-up and error messages.
                if (connectionSuccess){
                    Toast.makeText(getBaseContext(),R.string.login_failed,Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(getBaseContext(),R.string.connection_error,Toast.LENGTH_SHORT).show();
                }
            }
        }

    }


    // Class for Making Validation of the TextFields, readable
    // .. and making it abstract
    public abstract class TextValidator implements TextWatcher {
        private final TextView textView;

        public TextValidator(TextView textView) {
            this.textView = textView;
        }

        public abstract boolean validate(TextView textView);//, String text);

        @Override
        final public void afterTextChanged(Editable s) {
//            String text = textView.getText().toString();
            validate(textView);//, text);
        }

        @Override
        final public void beforeTextChanged(CharSequence s, int start, int count, int after) { /* Don't care */ }

        @Override
        final public void onTextChanged(CharSequence s, int start, int before, int count) { /* Don't care */ }
    }

    // 'Concrete Class' Extending abstract class-'TextValidator'
    public class ContactValidator extends TextValidator implements View.OnFocusChangeListener
    {
        public ContactValidator(TextView textView)
        {
            super(textView);
        }
        @Override
        public boolean validate(TextView textView)
        {
            if ((textView.getText().toString()).length() == 0)
            {
                textView.setError(textView.getHint()+" is required !!");
                return false;
            }
            else
            {
                textView.setError(null);
                return true;
            }
        }

        public void onFocusChange(View textView, boolean hasFocus)
        {
            if(!hasFocus)
            {
                validate((TextView)textView);
            }
        }
    }

    public class ContactValidatorFactory<T extends Activity>
    {
        private ViewGroup viewGroup;
        private View view;
        private List<View> possibleTextViews = new ArrayList<View>();

        public void setUpValidators(T t)
        {
            viewGroup = ((ViewGroup) t.findViewById(android.R.id.content));
            view = viewGroup.getChildAt(0);
//            int ie = 0;
            possibleTextViews = view.getFocusables(View.FOCUS_DOWN);

            for(View possibleTextView : possibleTextViews)
            {
                if(possibleTextView instanceof EditText)
                {
                    // calling ADD text change
                    ((EditText) possibleTextView).addTextChangedListener(
                            new ContactValidator((EditText) possibleTextView));
                    // calling SET text change
                    ((EditText) possibleTextView).setOnFocusChangeListener(
                            new ContactValidator((EditText) possibleTextView));
                }
            }
        }

        // Validation Method
        public boolean validate()
        {
            for (View possibleTextView : possibleTextViews)
            {
                if(possibleTextView instanceof EditText)
                {
                    String error = ((EditText) possibleTextView).getError()==null?null:((EditText) possibleTextView).getError().toString();
                    boolean validated = (error.length() == 0);

                    if(!validated) return false;
                }
            }
            return true;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(username.getWindowToken(),0);
        return true;
    }

}
