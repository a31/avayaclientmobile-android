package com.avaya.a31.avayaclientmobile.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FragmentDatePicker extends DialogFragment {
    private Date mDate;
    private int mYear, mMonth, mDay, mHour, mMin;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        mDate = Calendar.getInstance().getTime();
        calendar.setTime(mDate);
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMin = calendar.get(Calendar.MINUTE);

        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_date_picker, null);

        DatePicker datePicker = (DatePicker)v.findViewById(R.id.datePicker);
        TimePicker timePicker = (TimePicker)v.findViewById(R.id.timePicker);

        datePicker.init(mYear, mMonth, mDay, new DatePicker.OnDateChangedListener() {
            public void onDateChanged(DatePicker view, int year, int month, int day) {
                mYear = year;
                mMonth = month;
                mDay = day;
                updateDateTime();
            }
        });

        timePicker.setCurrentHour(mHour);
        timePicker.setCurrentMinute(mMin);
        timePicker.setOnTimeChangedListener(new TimePicker.OnTimeChangedListener() {
            public void onTimeChanged(TimePicker view, int hour, int min) {
                mHour = hour;
                mMin = min;
                updateDateTime();
            }
        });

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle("The date picker title")
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity mainActivity = (MainActivity) getActivity();
                        String userScopialink = getString(R.string.scopia_http_prefix).concat(mainActivity.userExtension);
                        //TODO: compose an email.
                        startActivity(IntentMaker.makeScheduleEmailIntent(
                                mainActivity.scopiaRecipients,userScopialink,mDate,
                                mainActivity.userFirstName,mainActivity.userLastName,mainActivity.userExtension,mainActivity.userEmail));
                    }
                })
                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Dialog canceled
                    }
                });
        return alertBuilder.create();
    }

    public void updateDateTime() {
        mDate = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMin).getTime();
    }
}
