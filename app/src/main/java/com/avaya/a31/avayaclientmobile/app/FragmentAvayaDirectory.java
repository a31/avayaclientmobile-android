package com.avaya.a31.avayaclientmobile.app;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FragmentAvayaDirectory extends Fragment implements SearchView.OnQueryTextListener{
    private SearchView searchView;
    private MenuItem ldapSearchItem;
    private ArrayList<NameValuePair> nameValuePairs; //Used for post request
    JSONArray searchResults;
    Activity mainActivity;
    private List<Contact> contacts;

    private ListView ldapListView;

    // Place to initiate list view
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle saveInstanceState){
        View view = inflater.inflate(R.layout.fragment_avaya_directory,container,false);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.avaya_directory_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
        ldapSearchItem = menu.findItem(R.id.search_action_ldap);
        searchView = (SearchView) ldapSearchItem.getActionView();
        searchView.setOnQueryTextListener(this);
    }

    public boolean onQueryTextSubmit(String searchStr){
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(searchView.getWindowToken(),0);
        nameValuePairs = new ArrayList<NameValuePair>();
        if(searchStr.length()>1){ // makesure The search string is at least two characters
            try{
                int num = Integer.parseInt(searchStr);
                nameValuePairs.add(new BasicNameValuePair("name",""));
                nameValuePairs.add(new BasicNameValuePair("ext",searchStr));
            }catch (NumberFormatException exception){
                //The search string is not purely numeric
                nameValuePairs.add(new BasicNameValuePair("name",searchStr));
                nameValuePairs.add(new BasicNameValuePair("ext",""));
            }
            searchView.setQuery("", false);
            searchView.clearFocus();
            MainActivity mainActivity = (MainActivity) getActivity();
            mainActivity.spinner.setVisibility(View.VISIBLE);
            new LdapSearchAsyncTask().execute();
        }else{
            Toast.makeText(getActivity().getBaseContext(), R.string.keyword_tooshort, Toast.LENGTH_SHORT).show();
        }
        return true;
    }
    public boolean onQueryTextChange(String searchStr){
        return false;
    }

    public class LdapSearchAsyncTask extends AsyncTask<Void,Integer,Void> {//Parameter, progress, results
        private boolean searchSuccess = false;
        @Override
        protected Void doInBackground(Void... params){
            try{
                HttpClient httpClient = new DefaultHttpClient(PublicMethods.httpParameters());
                HttpPost httpPost = new HttpPost(getString(R.string.ldap_search_url));
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
                HttpResponse httpResponse = httpClient.execute(httpPost);

                if(httpResponse.getStatusLine().getStatusCode()==200) {
                    HttpEntity httpEntity = httpResponse.getEntity();
                    if (httpEntity != null) {
                        InputStream inputStream = httpEntity.getContent();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                        //Create new JSON object and assign converted data
                        String jsonStr = reader.readLine(); //Read just one line
                        JSONObject jsonResponse = new JSONObject(jsonStr);
                        if (jsonResponse.getInt("status") == 0) {
                            searchResults = jsonResponse.getJSONArray("searchResultList");
                            searchSuccess=true;
                        }
                    }
                }
            }catch(Exception exception){
                exception.printStackTrace();
                //Cannot toast here because this is not the main thread.
                //Toast.makeText(getActivity().getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void unused){
            MainActivity mainActivity = (MainActivity)getActivity();
            mainActivity.spinner.setVisibility(View.GONE);

            int resultsCount;
            if (searchSuccess){
                if (searchResults == null) {
                    resultsCount = 0;
                } else {
                    resultsCount = searchResults.length();
                }
                String toastString;
                if (resultsCount>1){
                    toastString = String.format("%d results found.",resultsCount);
                }else{
                    toastString = String.format("%d result found.",resultsCount);
                }
                Toast.makeText(getActivity().getBaseContext(),toastString,Toast.LENGTH_SHORT).show();

                // Populate results in to listView
                contacts = new ArrayList<Contact>();// Recreate the list of contacts
                populateContactList();
                populateListView();
            }else{
                Toast.makeText(getActivity().getBaseContext(), R.string.connection_error, Toast.LENGTH_SHORT).show();
            }
        }

    }
    private void populateContactList(){
        //Create list of items, build the adapter, configure the list view.
        for (int i=0; i<searchResults.length();i++){
            try{
            JSONObject contact = searchResults.getJSONObject(i);
            contacts.add(new Contact(contact.getString("handle"),contact.getString("extension"),contact.getString("givenName"),contact.getString("familyName"),contact.getString("location"),contact.getString("email")));
            }catch (Exception exception){
                Toast.makeText(getActivity().getBaseContext(), R.string.missing_results, Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void populateListView(){
        //Fill the listView with content from Contact List
        ArrayAdapter<Contact> ldapListViewAdapter = new LdapListViewAdapter(getActivity().getBaseContext());
        ldapListView = (ListView) getView().findViewById(R.id.ldapListView);
        //------------ Adding keyboard logic ------------------------
        ldapListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(ldapListView.getWindowToken(),0);

                MainActivity mainActivity = (MainActivity) getActivity();
                PublicMethods.hideScopiaInbox(mainActivity);
                return false;
            }
        });
        ldapListView.setAdapter(ldapListViewAdapter);
    }

    @Override
    public void onAttach(Activity mainActivity) {
        super.onAttach(mainActivity);
        this.mainActivity = mainActivity;
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
    }

    private class LdapListViewAdapter extends ArrayAdapter<Contact>{
        LayoutInflater inflater;
        private MainActivity mainActivity = (MainActivity) getActivity();

        private View.OnClickListener callButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = ldapListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    startActivity(IntentMaker.makeCallIntent(contacts.get(position).getExtension()));
                }
            }
        };

        private View.OnClickListener emailButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = ldapListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    startActivity(IntentMaker.makeEmailIntent(contacts.get(position).getEmail()));
                }
            }
        };

        private View.OnClickListener scopiaButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = ldapListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    ArrayList<Contact> recipients = new ArrayList<Contact>();
                    recipients.add(contacts.get(position));
                    mainActivity.makeScopiaRequest(recipients);
                }
            }
        };

        private View.OnClickListener addContactButtonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final int position = ldapListView.getPositionForView(view);
                if (position != ListView.INVALID_POSITION) {
                    //Add contacts to database
                    Contact entry = contacts.get(position);

                    boolean addedNewContact = mainActivity.contactsDB.addContact(mainActivity.userHandle,entry);
                    if (addedNewContact){
                        Toast.makeText(getActivity().getBaseContext(), R.string.added_new_contact, Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity().getBaseContext(), R.string.updated_existing_contact, Toast.LENGTH_SHORT).show();
                    }

                    try {
                        mainActivity.updateContacts();
                    } catch (ClassCastException e) {
                        Toast.makeText(getActivity().getBaseContext(), R.string.system_failure, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };


        public LdapListViewAdapter(Context context){//The constructor
            super(context,R.layout.cell_ldap_search,contacts);
            inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View cellView = convertView;

            if (cellView == null){
                cellView = inflater.inflate(R.layout.cell_ldap_search, parent, false);
            }
            //Get the content from storage
            Contact contactEntry = contacts.get(position);

            //Fill the cellView
            //ImageButton callIconButton = (ImageButton) cellView.findViewById(R.id.callIconButton);
            TextView displayName = (TextView) cellView.findViewById(R.id.displayNameLabel);
            displayName.setText(contactEntry.getDisplayName());
            TextView location = (TextView) cellView.findViewById(R.id.locationLabel);
            location.setText(contactEntry.getLocation());

            Button callButton = (Button) cellView.findViewById(R.id.callButton);
            callButton.setText(contactEntry.getExtension());
            Button emailButton = (Button) cellView.findViewById(R.id.emailButton);
            emailButton.setText(contactEntry.getEmail());

            // Set on click listeners
            callButton.setOnClickListener(callButtonClickListener);
            emailButton.setOnClickListener(emailButtonClickListener);
            ImageButton callIconButton = (ImageButton) cellView.findViewById(R.id.callIconButton);
            ImageButton emailIconButton = (ImageButton) cellView.findViewById(R.id.emailIconButton);
            ImageButton scopiaButton = (ImageButton) cellView.findViewById(R.id.scopiaButton);
            ImageButton addContactButton = (ImageButton) cellView.findViewById(R.id.addContactButton);
            scopiaButton.setOnClickListener(scopiaButtonClickListener);
            callIconButton.setOnClickListener(callButtonClickListener);
            emailIconButton.setOnClickListener(emailButtonClickListener);
            addContactButton.setOnClickListener(addContactButtonClickListener);

            //return super.getView(position, convertView, parent);
            return cellView;
        }

    }

}
